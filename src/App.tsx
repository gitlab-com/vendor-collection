import React from 'react';
import { Main } from 'grommet'

import { Form } from './components'

function App() {
  return (
    <Main align='center'>
      <Form />
    </Main>
  );
}

export default App;
