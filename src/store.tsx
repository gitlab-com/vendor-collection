import { combineReducers } from 'redux'
import { configureStore } from '@reduxjs/toolkit'
import { reducer as formReducer } from 'redux-form'

const reducer = combineReducers({
  form: formReducer
})

const store = configureStore({
  reducer
})

export default store