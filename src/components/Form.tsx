import { Box, Button } from 'grommet'
import * as React from 'react'
import { Field as ReduxField, reduxForm } from 'redux-form'

import currencyOptions from '../currency.json'

import DateInput from './DateInput'
import Field from './Field'
import Select from './Select'

const currencies = Object.keys(currencyOptions)

const fields = [
  {
    component: Field,
    label: 'NetSuite Vendor Name',
    name: 'vendorName',
    placeholder: 'Name'
  },
  {
    component: Field,
    label: 'Line Quantity',
    name: 'lineQuantity',
    placeholder: '100',
    type: 'number'
  },
  {
    component: Field,
    label: 'Line Amount',
    name: 'lineAmount',
    placeholder: '25',
    type: 'number'

  },
  {
    component: Select,
    label: 'Currency',
    name: 'currency',
    options: currencies,
    placeholder: 'Select Currency'
  },
  {
    component: Field,
    label: 'Subsidiary',
    name: 'subsidiary',
  },
  {
    component: Field,
    label: 'GitLab Account',
    name: 'gitlabAccount'
  },
  {
    component: Field,
    label: 'Department',
    name: 'department'
  },
  {
    component: Field,
    label: 'Class',
    name: 'class'
  },
  {
    component: DateInput,
    label: 'Start Date',
    name: 'startDate',
    type: 'date'
  },
  {
    component: DateInput,
    label: 'End Date',
    name: 'endDate'
  },
  {
    component: Field,
    label: 'Requestor Name',
    name: 'requestorName'
  },
  {
    component: Field,
    label: 'Line Description',
    name: 'lineDescription'
  }
]

const VendorForm = () => {
  return (
    <Box width='large'>
      <form>
        {fields.map(({ name, component, ...props}) => 
          <ReduxField
            id={name}
            name={name}
            component={component}
            props={props}
          />)
        }
        <Button type='submit' label='Submit' primary />
      </form>
    </Box>
  )
}

export default reduxForm({
  form: 'vendorForm'
})(VendorForm)