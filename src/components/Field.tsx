import { FormField, TextInput } from 'grommet'
import * as React from 'react'

interface IField {
  name: string
  label: string
  placeholder: string
  type?: string
  input: any
}

const Field = (field: IField) => {
  return (
    <FormField id={field.name} label={field.label}>
      <TextInput
        value={field.input.value}
        onChange={(param: any) => field.input.onChange(param.value)}
        type={field.type || 'text'}
        placeholder={field.placeholder}
      />
    </ FormField>
  )
}

export default Field