import { FormField, MaskedInput } from 'grommet'
import * as React from 'react'

interface IDateInput {
  name: string
  label: string
  type?: string
  input: any
}

const dateMask = [
  {
    length: [4],
    options: ['2020', '2021', '2022', '2023', '2024', '2025', '2026'],
    placeholder: 'yyyy'
  },
  {
    fixed: '/'
  },
  {
    length: [2],
    options: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
    regexp: /^[0-5][0-9]$|^[0-9]$/,
    placeholder: 'mm'
  },
  {
    fixed: '/'
  },
  {
    length: [2],
    options: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
    regexp: /^[0-5][0-9]$|^[0-9]$/,
    placeholder: 'dd'
  }
]

const DateInput = (field: IDateInput) => {
  return (
    <FormField id={field.name} label={field.label}>
      <MaskedInput
        value={field.input.value}
        onChange={(param: any) => field.input.onChange(param.value)}
        mask={dateMask}
      />
    </FormField>
  )
}

export default DateInput