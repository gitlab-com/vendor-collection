import * as React from 'react'
import { FormField, Select } from 'grommet'

interface ISelectInput {
  name: string
  label: string
  placeholder: string
  type?: string
  input: any
  options: string[]
}

const SelectInput = (field: ISelectInput) => {
  const [opts, setOpts] = React.useState(field.options)

  function searchOptions(curr: string) {
    const newOpts = field.options.filter(opt => opt.toLowerCase().includes(curr.toLowerCase()))
    setOpts(newOpts)
  }

  return (
    <FormField id={field.name} label={field.label}>
      <Select 
        value={field.input.value}
        onChange={(param: any) => field.input.onChange(param.value)}
        options={opts}
        searchPlaceholder='Search'
        onSearch={searchOptions}
      />
    </FormField>
  )
}

export default SelectInput