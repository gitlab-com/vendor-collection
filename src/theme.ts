export default {
  global: {
    colors: {
      focus: '#FDBC60'
    },
    font: {
      family: 'Roboto',
    },
  },
  formField: {
    margin: {
      bottom: 'medium'
    }
  }
}